package pc.javier.seguime.control;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Javier 2019.
 */

public final class Aplicacion {
    public static AppCompatActivity actividadPrincipal;
    public static AppCompatActivity actividadSesion;
    public static AppCompatActivity actividadRegistros;
    public static AppCompatActivity actividadAyuda;
    public static AppCompatActivity actividadRegresiva;
    public static Context contexto;
}
