# <img src="metadata/es-AR/images/icon.png" alt="Icono" height="60"> Seguime

Almacena coordenadas GPS y las envía a un servidor web y así poder ver donde
esta el dispositivo.

* Podes montar tu propio servidor (Mayor privacidad).
* Envía coordenadas por Telegram (Bot) y SMS.
* Configurable desde el sitio web.
* Control sobre tiempos de actividad e inactividad para ahorrar batería.
* Temporizador para habilitar el modo de rastreo.
* Opción para iniciar con el sistema
* Cuentas fáciles de crear sin solicitar datos personales.


<img src="metadata/es-AR/images/phoneScreenshots/principal.jpg" alt="Captura de Pantalla" height="400">
<img src="metadata/es-AR/images/phoneScreenshots/menu.jpg" alt="Captura de Pantalla" height="400">
<img src="metadata/es-AR/images/phoneScreenshots/registros.jpg" alt="Captura de Pantalla" height="400">
<img src="metadata/es-AR/images/phoneScreenshots/temporizador.jpg" alt="Captura de Pantalla" height="400">

<img src="metadata/en-US/images/phoneScreenshots/principal2.jpg" alt="Captura de Pantalla" height="200">
<img src="metadata/en-US/images/phoneScreenshots/menu2.jpg" alt="Captura de Pantalla" height="200">
<img src="metadata/en-US/images/phoneScreenshots/registros2.jpg" alt="Captura de Pantalla" height="200">
<img src="metadata/en-US/images/phoneScreenshots/temporizador2.jpg" alt="Captura de Pantalla" height="200">


Es solo un proyecto, por lo tanto solo podrá usarse con fines experimentales y
no con fines de seguridad.

[<img src="https://f-droid.org/badge/get-it-on-es.png" alt="Disponible en F-Droid" height="80">](https://f-droid.org/app/pc.javier.seguime)

# Donaciones

Seguime es un proyecto para localizar un dispositivo con sistema operativo Android 

* Libre de Publicidad
* Libre de Rastreadores
* Libre de Telemetría
* Libre de Código Propietario
* Libre...

Al no tener publicidad este proyecto se mantiene únicamente con donaciones

[![Donaciones](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BDUHGWZKV2R8W)

# Mas información

Si querés mas información sobre este proyecto [Visita la Sección de Ayuda](https://seguime.000webhostapp.com/ayuda.php) en el sitio web.